Sparc Pattern Library
================

A HTML/CSS library for Sparcedge styles and modules.  

[View Sparcedge pattern library here](http://sparcedge.github.io/starter-pattern-library/). 

Source files are in the [gh_pages branch](https://github.com/sparcedge/starter-pattern-library/tree/gh-pages) of this repo.  

## Reusing this library 
This library is intended to be repurposed for other projects.  To create a new library based on this one, close the entire gh_pages branch of this repo, update the styles and snippets locally and reupload to your project repo. The library is compiling using javascript so the only thing that needs to be compiled locally in order to view in your browser is the project.less file.  

## Questions?
Have questions, comments? Contact:

* @shivika - Shivika Asthana
